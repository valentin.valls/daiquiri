#!/usr/bin/env python
import datetime
import logging
import os
import re
import subprocess


from setuptools import setup
from distutils.dist import Distribution


dist = Distribution()
dist.parse_config_files()


# Stolen from bliss/setup.py
def generate_release_file():
    dirname = os.path.dirname(os.path.abspath(__file__))
    try:
        process = subprocess.run(
            ["git", "describe", "--tags", "--always"], capture_output=True, cwd=dirname
        )
        if process.returncode:
            raise Exception("Not a git repository")
    except Exception:
        logging.error(
            "Error while executing git to get version. Generating a dummy version instead",
            exc_info=True,
        )
        version = "0.0.0+master"

    else:
        version = process.stdout.strip().decode()
        # Normalize to PEP 440
        version = version.replace("-", "+", 1)
        # Incase there are no tags
        if not re.match(r"^\d+\.\d+\.\d+", version):
            version = "0.0.0+" + version

    metadata = dist.get_option_dict("metadata")
    date = datetime.datetime.now()
    license_string = ""
    with open(metadata["license_file"][1]) as f:
        license_string = f.readline().strip()

    name = metadata["name"][1]
    author = metadata["author"][1]
    author_email = metadata["author_email"][1]
    license = license_string
    copyright = f"2019-{date.year} " + metadata["author"][1]
    description = metadata["description"][1]
    url = metadata["project_urls"][1].split("\n")[1].split(" = ")[1]

    src = f"""\
# -*- coding: utf-8 -*-
# Single source of truth for the version number and the like
import os
import re
import subprocess

dirname = os.path.dirname(os.path.abspath(__file__))

name = "{name}"
author = "{author}"
author_email = "{author_email}"
license = "{license}"
copyright = "{copyright}"
description = "{description}"
url = "{url}"
try:
    process = subprocess.run(
        ["git", "describe", "--tags", "--always"], capture_output=True, cwd=dirname
    )
    if process.returncode:
        raise Exception("Not a git repository")
except Exception:
    short_version = version = "{version}"
else:
    version = process.stdout.strip().decode()
    # Normalize to PEP 440
    version = version.replace("-", "+", 1)
    # Incase there are no tags
    if not re.match(r"^\d+\.\d+\.\d+", version):
        version = "0.0.0+" + version
    short_version = version

_package_version = version.split("+", 1)[0]
version_info = [int(v) for v in _package_version.split(".")]
"""
    with open(os.path.join(dirname, "daiquiri", "release.py"), "w") as f:
        f.write(src)
    return locals()


def main():
    generate_release_file()
    setup()


if __name__ == "__main__":
    main()
