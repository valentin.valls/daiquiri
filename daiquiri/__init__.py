# -*- coding: utf-8 -*-
"""daiquiri main package

.. autosummary::
    :toctree:

    core
"""
from . import release

__version__ = release.version
__author__ = release.author
__license__ = release.license
version_info = release.version_info
