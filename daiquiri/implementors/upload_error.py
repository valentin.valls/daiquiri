import time
from daiquiri.core.components import ComponentActor


class Upload_ErrorActor(ComponentActor):
    name = "upload_error"

    def method(self, *args, **kwargs):
        """Actor to process actor errors externally
        
        Kwargs:
            actid (str): The actor id
            actor (ComponentActor): The actor raising the exception
            exception (Exception): The exception the actor raised

        """
        print(kwargs)
        time.sleep(1)
