from daiquiri.core.components import ComponentActor


class MoveActor(ComponentActor):
    name = "move"

    def method(self, *args, **kwargs):
        kwargs["absol"]["move_to"](kwargs["absol"])

        if kwargs["absol"].get("positions"):
            kwargs["absol"]["move_to_additional"](kwargs["absol"]["positions"])
