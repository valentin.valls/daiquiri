#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from abc import ABC, abstractmethod

import yaml


def handler(exectype, inst, traceback):
    if isinstance(inst, PrettyException):
        print(inst.pretty())
    else:
        sys.__excepthook__(exectype, inst, traceback)


sys.excepthook = handler


class PrettyException(Exception, ABC):
    @abstractmethod
    def pretty(self):
        pass


class SyntaxErrorYAML(PrettyException):
    """YAML Syntax Error
    
    Args:
        error (obj): The YAMLError instance

    """

    def pretty(self):
        arg = self.args[0]
        message = "Invalid YAML Syntax\n"
        for l in str(arg["error"]).split("\n"):
            message += f"  {l}\n"
        return message


def indent(text, spaces=2):
    return "\n".join(
        ["{pad}{l}".format(l=l, pad=" " * spaces) for l in text.split("\n")]
    )


class InvalidYAML(PrettyException):
    """InvalidYAML Exception

    Args:
        message (str): Error message
        file (str): The yaml file name
        obj (dict): Parsed yaml dict
        errors (dict, optional): Error dict
    """

    def pretty(self):
        arg = self.args[0]
        message = f"Invalid YAML Definition in {arg['file']}\n"
        message += f"  {arg['message']}\n"
        message += indent(yaml.dump(dict(arg["obj"]), default_flow_style=False), 4)

        if "errors" in arg:
            message += "  Errors were:"
            for k, v in arg["errors"].items():
                message += f"    {k}:\n"
                if type(v) == list:
                    message += indent(yaml.dump(v, default_flow_style=False), 6)
                else:
                    message += indent(yaml.dump(dict(v), default_flow_style=False), 6)

        return message
