from daiquiri.core.metadata import MetaDataHandler


class IspyalchemyHandler(MetaDataHandler):
    exported = []

    def __init__(self, tables, session_scope, config):
        for n, t in tables.items():
            setattr(self, n, t)

        self.session_scope = session_scope
        self._config = config
