# -*- coding: utf-8 -*-
import logging

from daiquiri.core.authenticator import AuthenticatorInterface

logger = logging.getLogger(__name__)


class TestAuthenticator(AuthenticatorInterface):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        logger.debug("Loaded: {c}".format(c=self.__class__.__name__))

    def authenticate(self, username, password):
        if username in ["abcd", "efgh", "hijk"]:
            return True
        else:
            return False
