import numpy


def argsort(arr, axis=0):
    """Returns index that sorts arrays

    :param array arr:
    :returns tuple(list(int)):
    """
    idx = list(numpy.ix_(*[numpy.arange(i) for i in arr.shape]))
    idx[axis] = arr.argsort(axis)
    return tuple(idx)


def isDiagonal(arr):
    """Checks whether an array is diagonal

    :param array arr:
    """
    return numpy.count_nonzero(arr - numpy.diag(numpy.diagonal(arr))) == 0


def dtypeRange(dtype):
    """Data length of dtype

    :param dtype:
    :returns 2-tuple: min and max
    """
    dtype = numpy.array(0, dtype=dtype).dtype
    try:
        dinfo = numpy.finfo(dtype)
    except ValueError:
        try:
            dinfo = numpy.iinfo(dtype)
        except ValueError:
            return False, True
    return dinfo.min, dinfo.max


def makeFinite(arr, maxnum=None):
    """Make isFinite in-place

    :param num or array-like array arr:
    :param num maxnum:
    :returns array: same if input was ndarray
    """
    arr = numpy.asarray(arr, dtype="<f4")
    if maxnum is None:
        dmin, dmax = dtypeRange(arr.dtype)
    else:
        maxnum = numpy.asarray(maxnum, arr.dtype).item()
        dmin, dmax = -abs(maxnum), abs(maxnum)
    msk = arr == numpy.inf
    arr[msk] = dmax
    msk = arr == -numpy.inf
    arr[msk] = dmin
    return arr
