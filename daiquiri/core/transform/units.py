from pint import UnitRegistry

ureg = UnitRegistry()


def isquantity(x):
    return isinstance(x, ureg.Quantity)


def asquantity(value, units=None, convert=None):
    """
    :param Quantity or num or array value:
    :param Unit or str units: dimensionless by default
    :pram bool convert: by default convert when units not None
    :returns Quantity:
    """
    if isquantity(value):
        if convert is None:
            convert = units is not None
        elif convert:
            return value.to(units)
        else:
            return value
    else:
        return ureg.Quantity(value, units=units)


def asvalue(value, units):
    """
    :param Quantity or num or array value:
    :param Unit or str units:
    :returns num or array:
    """
    return asquantity(value, units, convert=True).magnitude


def get_exponent(unit, relative_to=ureg.m):
    """
    :param Unit str units:
    :param Relative to SI unit:
    :returns float exponent:
    """
    if not hasattr(ureg, unit):
        raise KeyError(f"No such unit {unit}")

    unit_obj = getattr(ureg, unit)
    return (1 * unit_obj).to(relative_to).magnitude
