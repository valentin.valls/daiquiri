#!/usr/bin/env python
# -*- coding: utf-8 -*-
import io
import logging
from flask import Response
from daiquiri.core import marshal
from daiquiri.core.components import Component, ComponentResource
from daiquiri.core.schema import ErrorSchema
from daiquiri.resources.utils import get_resource


logger = logging.getLogger(__name__)


class SynopticResource(ComponentResource):
    @marshal(out=[[404, ErrorSchema(), "Could not find svg"]])
    def get(self, **kwargs):
        """Get a synoptic image"""
        try:
            synoptic = self._parent.get_synoptic(kwargs["sid"])
            if synoptic:
                with open(synoptic, "rb") as svg:
                    data = io.BytesIO(svg.read())
                    resp = Response(
                        response=data, mimetype=f"image/svg+xml", status=200
                    )
                    resp.headers.add("Content-Length", data.getbuffer().nbytes)

                    return resp

            else:
                return {"error": "Could not find synoptic svg"}, 400

        except RuntimeError:
            logger.exception("Could not find synoptic")
            return {"error": "Could not find synoptic svg"}, 400


class Synoptic(Component):
    _config_export = ["synoptics"]

    def setup(self):
        self.register_route(SynopticResource, "/<int:sid>")

    def get_synoptic(self, sid):
        if sid < 0 or sid > len(self._config["synoptics"]) - 1:
            return

        fname = self._config["synoptics"][sid]["synoptic"]
        return get_resource("synoptic", fname)
