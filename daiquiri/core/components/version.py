#!/usr/bin/env python
# -*- coding: utf-8 -*-
import importlib
import logging
import daiquiri
from daiquiri.core import marshal
from daiquiri.core.components import Component, ComponentResource
from daiquiri.core.schema.components import VersionSchema


logger = logging.getLogger(__name__)


class VersionResource(ComponentResource):
    @marshal(out=[[200, VersionSchema(), "Current version information"]])
    def get(self):
        """Returns the current daiquiri version"""
        return self._parent.get_versions()


class Version(Component):
    """Version Component

    Simply returns the current app version
    """

    def setup(self):
        self.register_route(VersionResource, "")

    def get_versions(self):
        versions = {"version": daiquiri.__version__, "libraries": {}}

        for library in self._base_config["versions"]:
            try:
                mod = importlib.import_module(library)
                versions["libraries"][library] = mod.__version__
            except Exception:
                logger.warning(f"Could not get version for library: {library}")

        return versions
