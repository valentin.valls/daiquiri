#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields

from daiquiri.core.schema.validators import ValidatedRegexp
from daiquiri.core.schema.session import SessionListSchema

import logging

logger = logging.getLogger(__name__)


class LoginSchema(Schema):
    username = ValidatedRegexp("word", required=True)
    password = fields.Str(required=True)
    client = ValidatedRegexp("word-dash")


class LoginResponseSchema(SessionListSchema):
    token = fields.Str()
