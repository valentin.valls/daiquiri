from marshmallow import Schema, fields


class ComponentSchema(Schema):
    component = fields.Str(required=True)
