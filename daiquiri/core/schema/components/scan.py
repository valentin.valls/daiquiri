#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields
from daiquiri.core.schema.validators import OneOf
from daiquiri.core.hardware.abstract.scansource import ScanStates

import logging

logger = logging.getLogger(__name__)


class ScanShapeSchema(Schema):
    npoints1 = fields.Int()
    npoints2 = fields.Int()
    dim = fields.Int()
    requests = fields.Dict()


class ScanSchema(Schema):
    scanid = fields.Int(required=True)
    node_name = fields.Str()
    npoints = fields.Int()
    shape = fields.Nested(ScanShapeSchema)
    count_time = fields.Float()
    filename = fields.Str()
    start_timestamp = fields.Float()
    end_timestamp = fields.Float()
    estimated_time = fields.Float()
    status = OneOf(ScanStates)
    title = fields.Str()
    type = fields.Str()
    children = fields.List(fields.Dict())
    group = fields.Bool()


class ScanStatusSchema(Schema):
    scanid = fields.Int()
    progress = fields.Int()


class ScanDataSchema(Schema):
    scanid = fields.Int(required=True)
    axes = fields.Dict()
    data = fields.Dict()
    npoints = fields.Int()
    shape = fields.Nested(ScanShapeSchema)
    npoints_avail = fields.Int()
    pages = fields.Int()
    page = fields.Int()


class ScanSpectrumConversionSchema(Schema):
    zero = fields.Float()
    scale = fields.Float()


class ScanSpectraSchema(Schema):
    scanid = fields.Int()
    node_name = fields.Str()
    data = fields.Dict()
    npoints = fields.Int()
    npoints_avail = fields.Int()
    conversion = fields.Nested(ScanSpectrumConversionSchema)
