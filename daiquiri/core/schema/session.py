#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields

from daiquiri.core.schema.validators import ValidatedRegexp

import logging

logger = logging.getLogger(__name__)


class SessionListSchema(Schema):
    """Deliberately exclude token"""

    operator = fields.Bool(
        metadata={"description": "Denotes if this session is the current operator"}
    )
    last_access = fields.Float(
        metadata={"description": "Last epoch time that this session was seen"}
    )
    sessionid = fields.UUID(metadata={"description": "The uuid of the session"})
    data = fields.Dict()
    operator_pending = fields.Bool()
    mirrored = fields.Bool()


class SignSchema(Schema):
    url = ValidatedRegexp(
        "path", metadata={"description": "Url to sign"}, required=True
    )
    bewit = fields.Str(metadata={"description": "The signed bewit"})
