#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract import HardwareTranslator
from daiquiri.core.hardware.abstract.shutter import Shutter as AbstractShutter
from daiquiri.core.hardware.hwr.object import HWRObject

import logging

logger = logging.getLogger(__name__)


class Shutter(HWRObject, AbstractShutter):
    property_getter_map = {
        "state": "state",
        "valid": "is_valid",
        "open_text": "open_text",
        "closed_text": "closed_text",
    }

    property_setter_map = {"state": "set_state"}

    callable_map = {"open": "open", "close": "close"}

    def _call_toggle(self):
        if self._object.state() == self._object.STATE.OPEN.name:
            self._object.close()
        elif self._object.state() == self._object.STATE.CLOSED.name:
            self._object.open()
