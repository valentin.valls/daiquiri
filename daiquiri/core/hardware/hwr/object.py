#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract import GetterSetterHardwareObject

import logging

logger = logging.getLogger(__name__)


class HWRObject(GetterSetterHardwareObject):
    _protocol = "hwr"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self._object = kwargs.get("obj")
        for p in self.property_getter_map.values():
            logger.debug("connecting to {p} {obj}".format(p=p, obj=self._object))
            # connect to hwr events here

    # function to call when a HWR event happens
    def _event(self, value, *args, **kwargs):
        logger.debug("HWRObject._event {v} {kw}".format(v=value, kw=kwargs))
        # for p, v in self.property_map.items():
        #    pass
        # some magic
        # self._update(p, value)
