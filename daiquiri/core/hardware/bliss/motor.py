#!/usr/bin/env python
# -*- coding: utf-8 -*-
import math

from daiquiri.core.hardware.abstract import HardwareTranslator
from daiquiri.core.hardware.abstract.motor import Motor as AbstractMotor, MotorStates
from daiquiri.core.hardware.bliss.object import BlissObject

import logging

logger = logging.getLogger(__name__)


class BlissMotorTranslator(HardwareTranslator):
    def from_limits(self, value):
        value = list(value)
        for i, v in enumerate(value):
            if math.isinf(v):
                if v < 0:
                    value[i] = -999999999
                else:
                    value[i] = 999999999

        return value

    def from_state(self, value):
        # vals = value._current_states
        states = []
        for s in MotorStates:
            if s in value:
                states.append(s)

        if len(states):
            return states

        return ["UNKNOWN"]

    def from_position(self, value):
        if math.isnan(value):
            return None

        return round(value, 4)


class Motor(BlissObject, AbstractMotor):
    translator = BlissMotorTranslator

    property_map = {
        "position": "position",
        "tolerance": "tolerance",
        "acceleration": "acceleration",
        "velocity": "velocity",
        "limits": "limits",
        "state": "state",
        "unit": "unit",
    }

    callable_map = {"stop": "stop", "wait": "wait_move"}

    # Call moves with wait=False
    def _call_move(self, value, **kwargs):
        logger.debug(f"_call_move {self.name()} {value} {kwargs}")

        current = self.get("position")
        tolerance = self.get("tolerance")

        if abs(value - current) > tolerance:
            self._object.move(value, wait=False)
        else:
            logger.debug(
                f"Demand position ({value}) is within tolerance ({tolerance}) of current ({current})"
            )

    def _call_rmove(self, value, **kwargs):
        logger.debug(f"_call_rmove {self.name()} {value} {kwargs}")
        self._object.move(value, wait=False, relative=True)
