#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract import HardwareTranslator
from daiquiri.core.hardware.abstract.multiposition import (
    Multiposition as AbstractMultiposition,
    MultipositionStates,
)
from daiquiri.core.hardware.bliss.object import BlissObject

import logging

logger = logging.getLogger(__name__)


class BlissMPTranslator(HardwareTranslator):
    def from_positions(self, value):
        positions = []
        for l in value:
            target = [
                {
                    "object": t["axis"].name,
                    "destination": t["destination"],
                    "tolerance": t["tolerance"],
                }
                for t in l["target"]
            ]
            p = {
                "position": l["label"],
                "description": l["description"],
                "target": target,
            }
            positions.append(p)

        return positions

    def from_state(self, value):
        for s in MultipositionStates:
            if value == s:
                return s


class Multiposition(BlissObject, AbstractMultiposition):
    translator = BlissMPTranslator

    property_map = {
        "position": "position",
        "positions": "positions_list",
        "state": "state",
    }

    callable_map = {"stop": "stop"}

    def _call_move(self, value):
        logger.debug(f"_call_move multiposition {value}")
        self._object.move(value, wait=False)
