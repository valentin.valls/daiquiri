#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract.generic import Generic as AbstractGeneric
from daiquiri.core.hardware.rtschemamixin import RTSchemaMixin
from daiquiri.core.hardware.bliss.object import BlissObject

import logging

logger = logging.getLogger(__name__)


class Channelfromconfig(RTSchemaMixin, BlissObject, AbstractGeneric):
    _type = "config"

    property_map = {"state": "state"}
