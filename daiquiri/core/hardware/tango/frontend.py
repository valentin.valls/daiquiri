#!/usr/bin/env python
# -*- coding: utf-8 -*-
from tango import DevState

from daiquiri.core.hardware.abstract import HardwareTranslator
from daiquiri.core.hardware.abstract.frontend import Frontend as AbstractFrontend
from daiquiri.core.hardware.tango.object import TangoObject

import logging

logger = logging.getLogger(__name__)


class FrontendTranslator(HardwareTranslator):
    def itlk(self, value):
        val_map = {DevState.ON: "ON", DevState.FAULT: "FAULT"}

        for k, v in val_map.items():
            if k == value:
                return v

        return "UNKNOWN"

    def from_feitlk(self, value):
        return self.itlk(value)

    def from_pssitlk(self, value):
        return self.itlk(value)

    def from_expitlk(self, value):
        return self.itlk(value)

    def from_state(self, value):
        val_map = {
            DevState.OPEN: "OPEN",
            DevState.RUNNING: "RUNNING",
            DevState.CLOSE: "CLOSED",
            DevState.STANDBY: "STANDBY",
            DevState.FAULT: "FAULT",
            DevState.UNKNOWN: "UNKNOWN",
        }
        for k, v in val_map.items():
            if k == value:
                return v

        return "UNKNOWN"


class Frontend(TangoObject, AbstractFrontend):
    translator = FrontendTranslator

    property_map = {
        "state": "state",
        "status": "status",
        "automatic": "Automatic_Mode",
        "frontend": "FE_State",
        "current": "SR_Current",
        "mode": "SR_Mode",
        "refill": "SR_Refill_Countdown",
        "message": "SR_Operator_Mesg",
        "feitlk": "FE_Itlk_State",
        "pssitlk": "PSS_Itlk_State",
        "expitlk": "EXP_Itlk_State",
    }

    callable_map = {"open": "Open", "close": "Close", "reset": "Reset"}
