#!/usr/bin/env python
# -*- coding: utf-8 -*-
from tango import DevState

from daiquiri.core.hardware.abstract import HardwareTranslator
from daiquiri.core.hardware.abstract.pump import Pump as AbstractPump
from daiquiri.core.hardware.tango.object import TangoObject

import logging

logger = logging.getLogger(__name__)


class PumpTranslator(HardwareTranslator):
    def from_state(self, value):
        val_map = {DevState.ON: "ON", DevState.OFF: "OFF", DevState.UNKNOWN: "UNKNOWN"}
        for k, v in val_map.items():
            if k == value:
                return v

        return "UNKNOWN"


class Pump(TangoObject, AbstractPump):
    translator = PumpTranslator

    property_map = {
        "state": "state",
        "status": "status",
        "pressure": "pressure",
        "voltage": "voltage",
        "current": "current",
    }

    callable_map = {"on": "on", "off": "off"}
