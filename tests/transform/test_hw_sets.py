import pytest
import numpy
from daiquiri.core.transform import hw_sets


def test_motor_domain(motors):
    # Do not allow the upper limit for the piezo:
    stepperpiezo = [motors["simy"], motors["simpy"]]
    domain = hw_sets.MotorDomain(stepperpiezo, closed=[[True, True], [True, False]])

    # Check limits
    assert domain.motor_limits("simy") == (-4, 6)
    assert domain.motor_limits("simpy") == (0, 100)
    with pytest.raises(IndexError):
        domain.motor_limits("simx")

    # Check in/out domain
    assert domain.current_position in domain
    assert [[0, 0], [-1, 50], [3, 99]] in domain
    assert [[0, 0], [-1, 50], [3, 100]] not in domain
    assert [0, 0, 0] not in domain

    # Check destination and limits
    domain.current_position = [0, 50]
    domain.wait_motion_done()
    with pytest.raises(ValueError):
        domain.current_position = [0, 100]

    # Check move
    domain.move([1, 50])
    numpy.testing.assert_array_equal(domain.current_position, [1, 50])
    domain.rmove([-1, 10])
    numpy.testing.assert_array_equal(domain.current_position, [0, 60])
    with pytest.raises(ValueError):
        domain.rmove([1, 100])
    numpy.testing.assert_array_equal(domain.current_position, [0, 60])


def test_motor_composite_domain(motors):
    # Do not allow the upper limit for the piezo:
    stepperpiezo = [motors["simy"], motors["simpy"]]
    domain1 = hw_sets.MotorDomain(stepperpiezo, closed=[[True, True], [True, False]])
    stepperpiezo = [motors["simz"], motors["simpz"]]
    domain2 = hw_sets.MotorDomain(stepperpiezo, closed=[[True, True], [True, False]])
    domain3 = hw_sets.MotorDomain([motors["simx"]])
    domain = hw_sets.CompositeMotorDomain([domain1, domain2, domain3])

    # Check limits
    assert domain.motor_limits("simy") == (-4, 6)
    assert domain.motor_limits("simpy") == (0, 100)
    assert domain.motor_limits("simz") == (-1, 5)
    assert domain.motor_limits("simpz") == (0, 100)
    assert domain.motor_limits("simx") == (-2, 3)
    with pytest.raises(IndexError):
        domain.motor_limits("simpx")

    # Check in/out domain
    # simy, simpy, simz, simpz, simx
    assert domain.current_position in domain
    assert [[0, 0, 0, 0, 0], [1, 50, 2, 30, 2]] in domain
    assert [[0, 0, 0, 0, 0], [1, 50, 2, 100, 2]] not in domain
    assert [0, 0, 0, 0, 0, 0] not in domain

    # Check destination and limits
    domain.current_position = [1, 50, 2, 30, 2]
    domain.wait_motion_done()
    with pytest.raises(ValueError):
        domain.current_position = [1, 50, 2, 100, 2]

    # Check move
    domain.move([1.5, 60, 3, 20, 1])
    numpy.testing.assert_array_equal(domain.current_position, [1.5, 60, 3, 20, 1])
    domain.rmove([-1, 10, -1, -20, 0])
    numpy.testing.assert_array_equal(domain.current_position, [0.5, 70, 2, 0, 1])
    with pytest.raises(ValueError):
        domain.rmove([0, 40, 0, 0, 0])
    numpy.testing.assert_array_equal(domain.current_position, [0.5, 70, 2, 0, 1])
