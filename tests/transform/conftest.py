import pytest


@pytest.fixture
def motors(app):
    motors = ["simx", "simy", "simz", "simpy", "simpz"]
    return {k: app.hardware.get_object(k) for k in motors}
