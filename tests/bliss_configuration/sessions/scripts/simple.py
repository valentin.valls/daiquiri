from bliss.common.standard import *
from bliss.scanning import scan


def test_prepare():
    robz.move(5)
    omega.move(90)


def test_scan():
    test_prepare()
    ascan(robz, 0, 5, 5, 1, diode, diode2)


def simple_scan(**args):
    return ascan(
        args["motor"],
        args["motor_start"],
        args["motor_end"],
        args["npoints"],
        args["time"],
        *args["detectors"],
        run=args.get("run", True)
    )


def test_move(motor_ids=["m0", "m1", "m2"], repeats=5):
    motors = [config.get(m) for m in motor_ids]
    for i in range(repeats):
        for m in motors:
            m.move(0, wait=False)

        for m in motors:
            m.wait_move()

        for m in motors:
            m.move(40, wait=False)

        for m in motors:
            m.wait_move()
