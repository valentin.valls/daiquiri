import os
import yaml
import pytest

from daiquiri.core.exceptions import SyntaxErrorYAML

# To instantiate !include
import daiquiri.resources.utils  # noqa: F401


def test_include():
    with open("include.yml", "w") as inc:
        inc.write("include: true")

    with open("test.yml", "w") as f:
        f.write("value: !include include.yml")

    with open("test.yml") as yml:
        obj = yaml.safe_load(yml)
        assert obj["value"]["include"] is True

    os.unlink("include.yml")
    os.unlink("test.yml")


def test_include_ref():
    with open("include.yml", "w") as inc:
        inc.write("include: true")

    with open("test.yml", "w") as f:
        f.write("value: !include include.yml#include")

    with open("test.yml") as yml:
        obj = yaml.safe_load(yml)
        assert obj["value"] is True

    os.unlink("include.yml")
    os.unlink("test.yml")


def test_include_ref_no_key():
    with open("include.yml", "w") as inc:
        inc.write("include: true")

    with open("test.yml", "w") as f:
        f.write("value: !include include.yml#second")

    with open("test.yml") as yml:
        with pytest.raises(SyntaxErrorYAML) as e:
            yaml.safe_load(yml)

        assert "include.yml" in str(e.value.args[0]["error"])
        assert "no such key" in str(e.value.args[0]["error"])

    os.unlink("include.yml")
    os.unlink("test.yml")


def test_include_ref_wrong_extension():
    with open("test.yml", "w") as f:
        f.write("value: !include include.txt")

    with open("test.yml") as yml:
        with pytest.raises(SyntaxErrorYAML) as e:
            yaml.safe_load(yml)

        assert "test.yml" in str(e.value.args[0]["error"])
        assert "must have extension" in str(e.value.args[0]["error"])

    os.unlink("test.yml")


def test_include_syntax_error():
    with open("include.yml", "w") as inc:
        inc.write(
            """include: true
second moo"""
        )

    with open("test.yml", "w") as f:
        f.write("value: !include include.yml#include")

    with open("test.yml") as yml:
        with pytest.raises(SyntaxErrorYAML) as e:
            yaml.safe_load(yml)

        assert "include.yml" in str(e.value.args[0]["error"])

    os.unlink("include.yml")
    os.unlink("test.yml")


def test_include_ref_none_object():
    with open("include.yml", "w") as inc:
        # This is valid yaml but not an object
        inc.write("include true")

    with open("test.yml", "w") as f:
        f.write("value: !include include.yml#include")

    with open("test.yml") as yml:
        with pytest.raises(SyntaxErrorYAML) as e:
            yaml.safe_load(yml)

        assert "include.yml" in str(e.value.args[0]["error"])
        assert "not an object" in str(e.value.args[0]["error"])

    os.unlink("include.yml")
    os.unlink("test.yml")
