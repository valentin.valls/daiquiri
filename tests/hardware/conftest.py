import pytest

# TODO: parameterize to reuse a fixture for multiple backends
# test shutter (exists in bliss and tango)


@pytest.fixture
def omega(app):
    omega = app.hardware.get_object("omega")
    pos = omega.get("position")

    omega.move(100)
    omega.wait()

    yield omega

    omega.move(pos)
    omega.wait()


@pytest.fixture
def robz_limits(app):
    robz = app.hardware.get_object("robz")
    limits = robz.get("limits")
    pos = robz.get("position")

    robz.set("limits", [-5, 5])
    robz.move(0)
    robz.wait()

    yield robz

    robz.set("limits", limits)
    robz.move(pos)
    robz.wait()
