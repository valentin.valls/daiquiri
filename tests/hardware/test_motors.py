import pytest


def test_motors(omega):
    omega.move(100)
    omega.wait()
    assert omega.get("position") == 100


def test_motor_events(omega):
    pass
    # def cb(event)


def test_read_only(omega):
    with pytest.raises(AttributeError):
        omega.set("state", "readonly")


def test_motor_outside_limits(robz_limits):
    with pytest.raises(ValueError):
        robz_limits.move(-10)
        robz_limits.wait()
