import pytest


@pytest.mark.parametrize("sioclient", [None], indirect=True)
def test_sio_unauthenticated(app, sioclient):
    assert not sioclient.is_connected()


@pytest.mark.parametrize("auth_sioclient", [None], indirect=True)
def test_sio_authenticated(app, auth_client, auth_sioclient):
    assert auth_sioclient.is_connected()
