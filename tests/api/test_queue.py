def test_start_queue(auth_client, with_session, with_control):
    res = auth_client.put("/api/queue", payload={"state": True})

    assert res.status_code == 200


def test_start_queue_without_control(auth_client, with_session):
    res = auth_client.put("/api/queue", payload={"state": True})

    assert res.status_code == 400
