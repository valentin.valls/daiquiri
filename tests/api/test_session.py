def test_session(auth_client, with_session):
    res = auth_client.get("/api/session")
    assert res.status_code == 200
