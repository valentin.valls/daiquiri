# Tests that server is enforcing require_blsession to restict access to
# app if a session is not active


def test_session_not_required(auth_client):
    res = auth_client.get("/api/session/current")
    assert res.status_code == 200

    res = auth_client.get("/api/metadata/sessions")
    assert res.status_code == 200


def test_no_session(auth_client):
    res = auth_client.get("/api/hardware")

    assert res.status_code == 403


def test_with_session(auth_client, with_session):
    res = auth_client.get("/api/hardware")

    assert res.status_code == 200
