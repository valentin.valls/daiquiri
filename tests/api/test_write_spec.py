import os
import pytest


@pytest.mark.apispec
def test_write_spec(write_spec):
    assert os.path.exists(f"{os.path.dirname(__file__)}/../../doc/api/spec.json")
