import pytest


@pytest.fixture
def metadata(app):
    yield app.metadata
