#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Example client of the REST server using socketio
"""

import socketio
import logging

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class BaseNameSpace(socketio.ClientNamespace):
    def __init__(self, namespace, *args, **kwargs):
        super().__init__(namespace)
        self.__dict__.update(("_" + k, v) for k, v in kwargs.items())

    def on_change(self, data):
        print("{cls}.on_change".format(cls=self.__class__.__name__), data)


class SocketIOClient:
    base = "http://localhost"
    port = 8080
    _token = None

    def __init__(self, token=None, base=None, port=None):
        if base is not None:
            self.base = base

        if port is not None:
            self.port = port

        if token:
            self._token = token

        self._socketio = socketio.Client()

    def connect(self):
        self._socketio.connect(
            "{ip}:{port}?token={token}".format(
                ip=self.base, port=self.port, token=self._token
            )
        )

    def register_namespace(self, namespace):
        self._socketio.register_namespace(namespace)

    def run(self):
        self._socketio.wait()


if __name__ == "__main__":
    client = SocketIOClient()
    client.register_namespace(BaseNameSpace("/hardware"))
    client.register_namespace(BaseNameSpace("/scans"))
    client.connect()
    client.run()
