Resources allow for local customisation of configuration, layouts, actors, ..., etc. Resource folders will be searched in turn for a particular resource, i.e. a config file until one is found.

The server logs notify when a resource is being used, for example:
```
2020-08-17 16:24:48,777 daiquiri.resources.utils    INFO    Using resource 'daiquiri_id01/resources/config/imageviewer.yml'
```

and for an actor:
```
2020-08-17 16:24:48,880 daiquiri.core.utils INFO    Instantiated 'RoiscanActor' from daiquiri_id01.implementors.imageviewer.roiscan
```

The local beamline project i.e. `daiquiri_idxx` automatically adds its own resources folder to the server on startup. This allow daiquiri to import configuration and implementors outside of the root code tree.

The server can be started with a number of resource folders:
```
daiquiri-server --resource-folders=... --implementors=... ...
```
where

 * **resource-folder** is a comma separated list of folders of the server resources 
 * **implementors** refers to the module to load to find the implementors, i.e. `daiquiri_idxx`

The folder `daiquiri.resources` is a fallback when resources cannot be found in the provided resource folders (if any).

!!! info
    Normally the server will be started from the local beamline project launcher daiquiri-server-idxx which will automatically configure the above options
