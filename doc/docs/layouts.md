User Interface (UI) layouts are also defined in yaml and follow a simple tree structure. Details on the contents of the yaml file can be found in the Daiquiri UI documentation [Layout Manager](https://ui.gitlab-pages.esrf.fr/daiquiri-ui/#/Layout%20Manager) section

!!! example 
    Example layout files can be found in `daiquiri/resources/layouts`

Layouts are not cached so simply refreshing the UI will result in updates

!!! info 
    Layouts should be stored in the local beamline project i.e. `daiquiri_idxx/daiquiri_idxx/resources/layouts`
