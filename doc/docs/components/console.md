The console component allows the server to interact with another running process, the results of which are communicated to the client via WebSockets.

The basic configuration requires only a command to launch, and can be limited to staff members only
```yaml
command: bash
require_staff: true
```

The designated command will be executed with `/bin/bash -s` and so can be used to load conda environments and the likes by chaining commands as follows:
```yaml
command: . /opt/conda/etc/profile.d/conda.sh && conda activate test && bliss -s test_session
```

!!! warning
    Any console command will be run as the user running the Daiquiri server, this could have serious security implications. It is recommended that this component is always restricted to staff members with `require_staff`
