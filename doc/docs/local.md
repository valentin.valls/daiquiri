The cookiecutter [daiquiri-local](https://gitlab.esrf.fr/ui/daiquiri-local) project provides a central location to store configuration, layouts, and local beamline code. The cookiecutter will initialise a git repository in the project which should be pushed to a remote repository. 


The cookiecutter will create a project with a directory of the form:
```bash
daiquiri_<beamline_identifier>

# e.g.
daiquiri_id00
```

The project should then be installed into the daiquiri environment
```bash
cd daiquiri_id00
pip install -e .
```

Installation of the project will provide a new daiquiri launcher with the resources path and implementors module automatically configured. Dauiqiri can then be launched with:
```bash
daiquir-server-id00 --static-folder /path/to/daiquiri-ui/build
```

## Organisation

The local code repository is organised into two main folders `resources` and `implementors`

### Resources

The resources directory is where config and layouts are stored.
```bash
daiquiri_id00/daiquiri_id00/resources/config    # config yml files
daiquiri_id00/daiquiri_id00/resources/layout    # layout yml files
daiquiri_id00/daiquiri_id00/resources/synoptic  # synoptic images (can be > 1)
daiquiri_id00/daiquiri_id00/resources/certs     # can also store certificates here (uncommitted!)
```

### Implementors

The implementors directory is where local [Actors](actors.md) should live. These are organised by component name

```bash
daiquiri_id00/daiquiri_id00/implementors                            # Root implementors
daiquiri_id00/daiquiri_id00/implementors/upload_error.py            # A general upload_error actor
daiquiri_id00/daiquiri_id00/implementors/imageviewer/createmap.py   # A create map actor for the imageviewer component
```

#### upload_error.py

upload_error.py is a general actor that daiquiri will call if something goes wrong with running an actor. This allows for example daqiuiri to report error messages to a central logging system.
